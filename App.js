import React, { Component } from 'react';
import {TouchableOpacity,YellowBox, AsyncStorage, FlatList,SafeAreaView,StyleSheet,ScrollView, Image,ActivityIndicator, StatusBar,  Text, View } from 'react-native';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { ListItem, SearchBar,CheckBox,FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import {Appbar,DataTable,List,TouchableRipple,Card, Title,Button, TextInput,Paragraph,Avatar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import LinearGradient from 'react-native-linear-gradient';
import ReactNativeSettingsPage, {
  NavigateRow,
  SectionRow,
  SwitchRow,
  CheckRow,
  SliderRow
} from "react-native-settings-page";
const BLUE = "#501e69";
console.disableYellowBox = true;
const styles = StyleSheet.create({
  bigBlue: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  },
  logo:{
    alignContent:'center',
    justifyContent:'center'
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: 'white',
    marginBottom: 10,
  },
  name: {
    fontSize: 24,
    color: '#FFFFFF',
    fontWeight: '400',
  },
  label: {
    flex: 1,
    flexWrap: 'wrap',
  },
  line:{
    margin:10,
    color:'#34afc2',
  },
  userInfo: {
    fontSize: 39,
    color: '#FFFFFF',
    fontWeight: '600',
  },
  body: {
    backgroundColor: '#778899',
    height: 500,
    alignItems: 'center',
  },
  item: {
    flexDirection: 'row',
  },
  infoContent: {
    flex: 1,
    alignItems: 'flex-start',
    paddingLeft: 5,
  },
  iconContent: {
    flex: 1,
    alignItems: 'flex-end',
    paddingRight: 5,
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 20,
    color: '#FFFFFF',
  },
  linearGradient: {
    width:400,
  },
  lineStyle:{
    borderWidth: 0.5,
    borderColor:'#34afc2',
    marginTop:7,
    margin:10,
},
  headerContent: {
    padding: 30,
    alignItems: 'center',
  },
  red: {
    color: 'red',
  },
  container: {
    flex: 1,
  
    justifyContent: 'center',
    backgroundColor: '#defafa',
   
  },
  header:{
    backgroundColor: '#2d375e',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  input: {
    margin:15,
    height:40,
    padding:5,
    fontSize:15,
    backgroundColor:'#defafa',
    borderBottomColor:'#42baf8',
  },
  btnEnter: {
    justifyContent:'center',
    flexDirection: 'row',
    backgroundColor: '#42Baf8',
    alignItems: 'center',
    marginLeft: 15,
    marginRight: 15,
    padding:10,
    borderBottomEndRadius:10,
    borderTopEndRadius:10,
    borderTopLeftRadius:10,
    borderBottomLeftRadius:10
  },
  btnEnterText: {
    color: '#ffffff',
    fontWeight:'700',
    fontSize:15,
  },
  checkboxContainer: {
    paddingRight: 8,
    position:"relative"
  },
  label: {
    flex: 1,
    flexWrap: 'wrap',
  },
  Inputaja: {
    height:60,
    paddingLeft:6,
    marginTop:10
  },
  list:{
    paddingVertical: 4,
    margin: 5,
    backgroundColor: "#fff"
   },
   lightText:{
    marginTop:10,
    fontWeight:'100',
    fontSize:16
   },
   logo: {
    width: 200,
    height:200,
    marginBottom:10,
  },
   lightUi:{
    marginTop:10,
    fontWeight:'100',
    fontSize:17
   },
  row: {
    minHeight: 48,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
});
const userInfo = {username: '', password:''};
var SampleArray = [] ;
var optionData = [];
class SplashScreen extends React.Component {
  static navigationOptions = {
    headerShown:false
   };
  render() {
    const viewStyles = {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: '#326fa8',
      justifyContent: 'center',
      alignItems: 'center',
    };
    const textStyles = {
      color: 'white',
      fontSize: 23,
      fontWeight: 'bold',
      justifyContent:'center',
      alignItems: 'center',
    };
    return (
      <View style={viewStyles}>
           <Image
        style={styles.logo}
        source={require('./icon/logo.png')}
    />
        <Text style={textStyles}>
         ABSENSI SISWA
        </Text>
        <Text style={textStyles}>
         SMP YPI PULOGADUNG
        </Text>
      </View>
    );
  }
}
class LoginScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      username:'',
      password:'', isLoading: true}
  }
  performTimeConsumingTask = async() => {
    return new Promise((resolve) =>
      setTimeout(
        () => { resolve('result') },
        2000
      )
    );
  }
  async componentDidMount() {
    const data = await this.performTimeConsumingTask();
    if (data !== null) {
      this.setState({ isLoading: false });
    }
  }
   static navigationOptions = {
    headerShown:false
   };
  render() {
    if (this.state.isLoading) {
      return <SplashScreen />;
    }
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Login To My Account !</Text>
        <TextInput 
          style={styles.input}
          placeholder="Username"
          onChangeText={(username) =>this.setState({username})}
          value={this.state.username}
        />
         <TextInput 
         style={styles.input}
          placeholder="Password"
          onChangeText={(password) =>this.setState({password})}
          value={this.state.password}
          secureTextEntry={true}
        />
        <TouchableOpacity style={styles.btnEnter} onPress={this._signin}>
            <Text style={styles.btnEnterText}>Sign To Login</Text>
        </TouchableOpacity>
      </View>
    );
  }
  _signin = async () => {
  let details = {
    'username': this.state.username,
    'password': this.state.password
};
let formBody = [];
    for (let property in details) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    fetch('http://absensi.dewonebeauty.com/api/auth', {
      method: 'POST',
      headers: {
          'Authorization': 'Bearer token',
          'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: formBody
  }).then((response) => response.json())
      .then((responseData) => {
          if (responseData != 'Gagal') {
            this.props.navigation.navigate('Home')
          }else {
              this.setState({ spinner: false });
              setTimeout(() => {
                alert('Username / Password Salah!');
              }, 100); 
          }
      })
      .done();
}
}
class HomeScreen extends React.Component{
  static navigationOptions = {
    headerShown:false
   };
   constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      error: null,
      date: '',
    };

    this.arrayholder = [];
  }

  addItem(item) {
    this.setState({item});
    console.log(item);
  }

  componentDidMount() {
    this.makeRemoteRequest();
    var that = this;
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    that.setState({
      date:
        date + '/' + month + '/' + year + ' ' + hours + ':' + min + ':' + sec,
    });
  }
  makeRemoteRequest = () => {
    const url = 'http://absensi.dewonebeauty.com/api/get_jadwal_hari_ini_by_guru/2';
    this.setState({ loading: true });
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: res,
          error: res.error || null,
          loading: false,
        });
        this.arrayholder = res;
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%',
        }}
      />
    );
  };

  searchFilterFunction = text => {
    this.setState({
      value: text,
    });
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.nama_kelas.toUpperCase()} ${item.nama_pelajaran.toUpperCase()} ${item.jam}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };
    render(){
    
      if (this.state.loading) {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator />
          </View>
        );
      }
      return (
          <View style={{flex: 1}}>
             <Appbar.Header style={styles.header}>
                <Appbar.Content
                  title="Jadwal Hari Ini"
                  subtitle="List Jadwal Mengajar Hari Ini"
                />
                <Appbar.Action onPress={() => this.props.navigation.navigate('Auth')} icon="settings"  />
              </Appbar.Header>
            <View style={{flex:1, backgroundColor:'white'}}>
            <List.Section>
                <List.Subheader>{this.state.date}</List.Subheader>
              </List.Section>
            <SafeAreaView>
            <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                      <ListItem 
                      button onPress={() => this.props.navigation.navigate('Kehadiran', { 'addItem': (item) => this.addItem(item) })}
                      leftIcon={{ name: 'apps'}}
                      rightIcon={{ name: 'send'}}
                      title={`${item.nama_pelajaran}`}
                      subtitle={`Kelas : ${item.nama_kelas} - Jam : ${item.jam}`}
                      />
                    )}
                    keyExtractor={item => item.jam}
                    ItemSeparatorComponent={this.renderSeparator}
                    ListHeaderComponent={this.renderHeader}
                />
          </SafeAreaView>
    </View>
            <View style={{backgroundColor:'white', height: 54, flexDirection:'row'}}>
                  <View style={{flex: 1, alignItems:'center', justifyContent:'center'}}>
                      <TouchableOpacity>
                      <View style={{width:26, height:26}}>
                    <Image style={{height:26, width:26}} source={require('./icon/home-active.png')} />
                    </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Home</Text>
                      </TouchableOpacity>
                  </View>
                  {/* <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Kehadiran',)}>
                       <View style={{width:26, height:26}}>
                          <Image style={{height:26, width:26}} source={require('./icon/order.png')} />
                       </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Kehadiran</Text>
                      </TouchableOpacity>
                  </View> */}
                  <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Jadwal')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/help.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Jadwal</Text>
                      </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Report')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/inbox.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Report</Text>
                      </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, alignItems:'center', justifyContent:'center' }}>
                  <TouchableOpacity  onPress={() => this.props.navigation.navigate('User')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/account.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Account</Text>
                      </TouchableOpacity>
                  </View>
             </View>
          </View>
      );
    }
}
class JadwalScreen extends React.Component{
  static navigationOptions = {
    headerShown:false
   };
   constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      error: null,
      date: '',
    };

    this.arrayholder = [];
  }

  componentDidMount() {
    this.makeRemoteRequest();
    var that = this;
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    that.setState({
      date:
        date + '/' + month + '/' + year + ' ' + hours + ':' + min + ':' + sec,
    });
  }
  makeRemoteRequest = () => {
    const url = 'http://absensi.dewonebeauty.com//api/get_jadwal_minggu_ini_by_guru/2';
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: res,
          error: res.error || null,
          loading: false,
        });
        this.arrayholder = res;
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%',
        }}
      />
    );
  };

  searchFilterFunction = text => {
    this.setState({
      value: text,
    });
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.nama_kelas.toUpperCase()} ${item.nama_pelajaran.toUpperCase()} ${item.hari.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };
    render(){
      if (this.state.loading) {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator />
          </View>
        );
      }
      return (
          <View style={{flex: 1}}>
             <Appbar.Header style={styles.header}>
                <Appbar.Content
                  title="Daftar Jadwal"
                  subtitle="List Jadwal Minggu Ini"
                />
                <Appbar.Action onPress={() => this.props.navigation.navigate('Auth')} icon="settings"  />
              </Appbar.Header>
            <View style={{flex:1, backgroundColor:'white'}}>
            <List.Section>
                <List.Subheader>{this.state.date}</List.Subheader>
              </List.Section>
            <SafeAreaView>
           
            <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <ListItem
              leftIcon={{ name: 'apps'}}
              title={`${item.nama_pelajaran}`}
              subtitle={`Kelas : ${item.nama_kelas} - Hari : ${item.hari}`}
            />
          )}
          keyExtractor={item => item.jadwal_id}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
        />
         
          </SafeAreaView>
    </View>
            <View style={{backgroundColor:'white', height: 54, flexDirection:'row'}}>
                  <View style={{flex: 1, alignItems:'center', justifyContent:'center'}}>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} >
                      <View style={{width:26, height:26}}>
                    <Image style={{height:26, width:26}} source={require('./icon/home.png')} />
                    </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Home</Text>
                      </TouchableOpacity>
                  </View>
                  {/* <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Kehadiran')}>
                       <View style={{width:26, height:26}}>
                          <Image style={{height:26, width:26}} source={require('./icon/order.png')} />
                       </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Kehadiran</Text>
                      </TouchableOpacity>
                  </View> */}
                  <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Jadwal')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/help-active.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Jadwal</Text>
                      </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Report')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/inbox.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Report</Text>
                      </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, alignItems:'center', justifyContent:'center' }}>
                  <TouchableOpacity  onPress={() => this.props.navigation.navigate('User')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/account.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Account</Text>
                      </TouchableOpacity>
                  </View>
             </View>
          </View>
      );
    }
}

class DaftarScreen extends React.Component{
  static navigationOptions = {
    headerShown:false
   };
   constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      error: null,
      date: '',
    };

    this.arrayholder = [];
  }

  componentDidMount() {
    this.makeRemoteRequest();
    var that = this;
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    that.setState({
      date:
        date + '/' + month + '/' + year + ' ' + hours + ':' + min + ':' + sec,
    });
  }
  makeRemoteRequest = () => {
    const url = 'http://absensi.dewonebeauty.com/api/get_report_absensi_detail_id/2  ';
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: res,
          error: res.error || null,
          loading: false,
        });
        this.arrayholder = res;
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%',
        }}
      />
    );
  };

  searchFilterFunction = text => {
    this.setState({
      value: text,
    });
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.nama_kelas.toUpperCase()} ${item.nama_pelajaran.toUpperCase()} ${item.jam}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };
    render(){
      if (this.state.loading) {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator />
          </View>
        );
      }
      return (
          <View style={{flex: 1}}>
             <Appbar.Header style={styles.header}>
             <Appbar.BackAction icon="back" 
            onPress={() => this.props.navigation.navigate('Report')}
            />
                <Appbar.Content
                  title="History"
                  subtitle="List Absensi Mengajar Siswa"
                />
                <Appbar.Action onPress={() => this.props.navigation.navigate('Auth')} icon="settings"  />
              </Appbar.Header>
            <View style={{flex:1, backgroundColor:'white'}}>
           
            <SafeAreaView>
            <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <ListItem 
            leftAvatar={{ source: { uri: 'https://www.pngkit.com/png/detail/244-2448974_remote-controlled-office-factory-lock-inr-22500.png' } }}
            title={`${item.nama_siswa}`}
            subtitle={`Kehadiran : ${item.absensi.toUpperCase()} `}
            />
          )}
          keyExtractor={item => item.kehadiran_id}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
        />
          </SafeAreaView>
    </View>
            <View style={{backgroundColor:'white', height: 54, flexDirection:'row'}}>
                  
             </View>
          </View>
      );
    }
}
class ReportScreen extends React.Component{
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitleStyle: {textAlign: "center",flex: 1},
      headerShown:false
     };
    };
    constructor(props) {
     super(props);
     this.state = {
       loading: true,
       dataSource:[]
      };
    }
    componentDidMount(){
    fetch("http://absensi.dewonebeauty.com/api/get_report_absensi_hari_ini_by_guru_id/2")
    .then(response => response.json())
    .then((responseJson)=> {
      this.setState({
       loading: false,
       dataSource: responseJson
      })
    })
    .catch(error=>console.log(error)) //to catch the errors if any
    }
    FlatListItemSeparator = () => {
    return (
      <View style={{
         height: 5,
         width:"100%",
        
    }}
    />
    );
    }
    renderItem=(data)=>
    <TouchableOpacity onPress={() => this.props.navigation.navigate('Daftar')} style={styles.list}>
    <Text style={styles.lightText}>Mata Pelajaran : {data.item.nama_pelajaran}</Text>
    <Text style={styles.lightText}>Jam   : {data.item.jam}</Text>
    <Text style={styles.lightText}>Kelas : {data.item.nama_kelas}</Text>
    </TouchableOpacity>
    render(){
      if(this.state.loading){
        return( 
          <View style={styles.loader}> 
          <ActivityIndicator size="large" color="#0c9"/>
          </View>
      )}
      return (
          <View style={{flex: 1}}>
             <Appbar.Header style={styles.header}>
             <Appbar.Content
                title="Report History"
                subtitle="List Report History Absensi"
              />
             <Appbar.Action onPress={() => this.props.navigation.navigate('Auth')} icon="settings"  />
              </Appbar.Header>
              <FlatList
                  data= {this.state.dataSource}
                  ItemSeparatorComponent = {this.FlatListItemSeparator}
                  keyExtractor= {item=>item.jadwal_id} 
                     renderItem= {item=> this.renderItem(item)}
                  
                  />
            <View style={{flex:1}}>
            <View>
        
        </View>
    </View>
            <View style={{backgroundColor:'white', height: 54, flexDirection:'row'}}>
                  <View style={{flex: 1, alignItems:'center', justifyContent:'center'}}>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                      <View style={{width:26, height:26}}>
                    <Image style={{height:26, width:26}} source={require('./icon/home.png')} />
                    </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Home</Text>
                      </TouchableOpacity>
                  </View>
                  {/* <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Kehadiran')}>
                       <View style={{width:26, height:26}}>
                          <Image style={{height:26, width:26, alignItems:'center'}} source={require('./icon/order.png')} />
                       </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Kehadiran</Text>
                      </TouchableOpacity>
                  </View> */}
                  <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Jadwal')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/help.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Jadwal</Text>
                      </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Report')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/inbox-active.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Report</Text>
                      </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, alignItems:'center', justifyContent:'center' }}>
                  <TouchableOpacity  onPress={() => this.props.navigation.navigate('User')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/account.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Account</Text>
                      </TouchableOpacity>
                  </View>
             </View>
          </View>
      );
    }
}
class UserScreen extends React.Component{
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitleStyle: {textAlign: "center",flex: 1},
      headerShown:false
     };
    };
    constructor(props) {
     super(props);
     this.state = {
       loading: true,
       dataSource:[]
      };
    }
    componentDidMount(){
    fetch("http://absensi.dewonebeauty.com/api/get_profile_guru_by_guru_id/2")
    .then(response => response.json())
    .then((responseJson)=> {
      this.setState({
       loading: false,
       dataSource: responseJson
      })
    })
    .catch(error=>console.log(error)) //to catch the errors if any
    }
    FlatListItemSeparator = () => {
    return (
      <View style={{
         height: 5,
         width:"100%",
        
    }}
    />
    );
    }
    renderItem=(data)=>
    <TouchableOpacity style={styles.list}>
  
    <Text style={styles.lightText}>Nama Lengkap : {data.item.nama_lengkap}</Text>
    <Text style={styles.lightText}>Username   : {data.item.username}</Text>
    <Text style={styles.lightText}>E-mail : {data.item.email}</Text>
    <Text style={styles.lightText}>No.Handphone : {data.item.no_telp}</Text>
    <Text style={styles.lightText}>Alamat : {data.item.alamat}</Text>
    </TouchableOpacity>
    render(){
      if(this.state.loading){
        return( 
          <View style={styles.loader}> 
          <ActivityIndicator size="large" color="#0c9"/>
          </View>
      )}
      return (
          <View style={{flex: 1}}>
             <Appbar.Header style={styles.header}>
             <Appbar.Content
                title="Profile"
              />
              <Appbar.Action onPress={() => this.props.navigation.navigate('Auth')} icon="settings"  />
              </Appbar.Header>
              <FlatList
                  data= {this.state.dataSource}
                  ItemSeparatorComponent = {this.FlatListItemSeparator}
                  keyExtractor= {item=>item.guru_id} 
                     renderItem= {item=> this.renderItem(item)}
                  
                  />
            <View style={{flex:1}}>
            <View>
        
        </View>
    </View>
            <View style={{backgroundColor:'white', height: 54, flexDirection:'row'}}>
                  <View style={{flex: 1, alignItems:'center', justifyContent:'center'}}>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                      <View style={{width:26, height:26}}>
                    <Image style={{height:26, width:26}} source={require('./icon/home.png')} />
                    </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Home</Text>
                      </TouchableOpacity>
                  </View>
                  {/* <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Kehadiran')}>
                       <View style={{width:26, height:26}}>
                          <Image style={{height:26, width:26, alignItems:'center'}} source={require('./icon/order.png')} />
                       </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Kehadiran</Text>
                      </TouchableOpacity>
                  </View> */}
                  <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Jadwal')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/help.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Jadwal</Text>
                      </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Report')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/inbox.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Report</Text>
                      </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, alignItems:'center', justifyContent:'center' }}>
                  <TouchableOpacity  onPress={() => this.props.navigation.navigate('User')}>
                  <View style={{width:26, height:26}}>
                     <Image style={{height:26, width:26}} source={require('./icon/account-active.png')} />
                  </View>
                      <Text style={{fontSize: 10, color: '#545454',marginTop:4}}>Account</Text>
                      </TouchableOpacity>
                  </View>
             </View>
          </View>
      );
    }
}

class KehadiranScreen extends React.Component {
  
  static navigationOptions = {
    headerShown:false
   };
constructor(props) {
    super(props);
    this.state = {
        radioBtnsData: ['Item1', 'Item2', 'Item3'],
        checked: 0,
        dataSource:[],
        data: [],
        tampung:[],
        listSiswa:'',
        Holder: '',
        Kelas: '',
    }
 
}

  state = {
    listSiswa:'',
    materi: '',
    catatan:'',
    checked: false,
    one:false,
    two:false,
    checked: 'first',
    selectedOption: [],
  };

  componentDidMount() {
    this.makeRemoteRequest();
    var that = this;
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    that.setState({
      date:
        date + '/' + month + '/' + year + ' ' + hours + ':' + min + ':' + sec,
    });
  }
  makeRemoteRequest = () => {
    const url = 'http://absensi.dewonebeauty.com/api/get_daftar_kelas_siswa_by_jadwal_id/2';
    this.setState({ loading: true });
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: res,
          error: res.error || null,
          loading: false,

        });
       
       
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%',
        }}
      />
    );
  };

  AddItemsToArray=()=>{
    SampleArray.push(this.state.Holder.toString());
    optionData.push(this.state.listSiswa.toString());
}
onRadiochange(value,siswa,jadwal,materi,catatan){
    let details = {
      'value': value,
      'siswa': siswa,
      'jadwal': jadwal,
      'materi': materi,
      'catatan': catatan
  };
   let formBody = [];
      for (let property in details) {
          let encodedKey = encodeURIComponent(property);
          let encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");
     

    fetch('http://absensi.dewonebeauty.com/api/submit_absensi', {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer token',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: formBody
    }).then((response) => response.json())
        .then((responseData) => {
            if (responseData != 'sukses') {
              setTimeout(() => {
                alert('Username / Password Salah!');
              }, 100);
            }else {
                this.setState({ spinner: false });
            }
        })
        .done();
};

render() {
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <ScrollView>
    <View>
        <View>
            <Appbar.Header style={styles.header}>
            <Appbar.BackAction icon="back" 
            onPress={() => this.props.navigation.navigate('Home')}
            />
                <Appbar.Content
                  title="Absensi Siswa"
                />
               <Appbar.Action onPress={() => this.props.navigation.navigate('Auth')} icon="settings"  />
              </Appbar.Header>
              <LinearGradient 
                start={{x: 0, y: 0}} 
                end={{x: 1, y: 0}} 
                colors={['#5D9EFF', '#2F548B', '#051937',]} 
                style={styles.linearGradient}>
              </LinearGradient>
             <View>
                    <TextInput
                    label='Materi Ajar'
                    value={this.state.materi}
                    selectionColor={BLUE}
                    style={styles.Inputaja}
                    onChangeText={materi => this.setState({ materi })}
                  />
            <TextInput
                    label='Catatan Kelas'
                    value={this.state.catatan}
                    selectionColor={BLUE}
                    style={styles.TextInput}
                    onChangeText={catatan => this.setState({catatan })}
                  />
                  <Text style={styles.line}>
                      Kehadiran Siswa
                    </Text>
                  <View style={styles.lineStyle}>
        </View>
        </View>
      </View>
          <View>
          <SafeAreaView style={{flex: 1}}>
           <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                      <View>
                      <ListItem 
                      button onPress={() => this.props.navigation.navigate('Kehadiran','Item' )}
                      leftAvatar={{ source: { uri: 'https://www.pngkit.com/png/detail/244-2448974_remote-controlled-office-factory-lock-inr-22500.png' } }}
                      title={`${item.nama_siswa}`}/>
                       <View style={styles.container}>
    
    <RadioGroup
      onSelect = {(index,value,jadwal_id,materi,catatan) => this.onRadiochange(value,item.siswa_id, item.jadwal_id,this.state.materi,this.state.catatan)}
      highlightColor='white'
    >
      <RadioButton value={'hadir'} >
        <Text>Hadir</Text>
      </RadioButton>
      <RadioButton value={'sakit'}>
        <Text>Sakit</Text>
      </RadioButton>
      <RadioButton value={'izin'}>
        <Text>Izin</Text>
      </RadioButton>
      <RadioButton value={'alpa'}>
        <Text>Alpa</Text>
      </RadioButton>
    </RadioGroup>
  </View> 
                      </View>                    
                    )}
                    keyExtractor={item => item.siswa_id}
                    ItemSeparatorComponent={this.renderSeparator}
                    ListHeaderComponent={this.renderHeader}
            />
             </SafeAreaView>
           
            </View>
            <Button icon="book" mode="contained" onPress={this._signin}  style={styles.header} >Simpan Absensi</Button>
    </View>
    </ScrollView>
    );
  }
  _signin = async () => {
    setTimeout(() => {
      alert('Absen Berhasil !');
    }, 100); 
    this.props.navigation.navigate('Home')
  }
}
class AuthLoadingScreen extends React.Component{
  constructor(props){
    super(props);
    this._loadData();
  }
  render(){
    return (
        <View style={styles.container}>
          <ActivityIndicator />
          <StatusBar barStyle="default" />
        </View>
    );
  }
  _loadData = async () =>{
    const logged = await AsyncStorage.getItem('Logged');
    this.props.navigation.navigate(logged !== '1'  ? 'Auth' : 'App' );
  }
}
const AppStack =  createStackNavigator({Home : HomeScreen});
const AuthStack =  createStackNavigator({Login : LoginScreen});
const KehadiranStack = createStackNavigator({Detail : KehadiranScreen});
const JadwalStack = createStackNavigator({Jadwal : JadwalScreen});
const ReportStack = createStackNavigator({Report : ReportScreen});
const UserStack = createStackNavigator({User : UserScreen});
const DaftarStack = createStackNavigator({Daftar : DaftarScreen});
export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading:AuthLoadingScreen,
      App: AppStack,
      Auth:AuthStack,
      Kehadiran:KehadiranStack,
      Jadwal: JadwalStack,
      Report: ReportStack,
      User: UserStack,
      Daftar:DaftarStack,
    },
    {
      headerShown:false,
      initialRouteName: 'Auth'
    }
  ),
);